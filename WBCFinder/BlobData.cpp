#include "stdafx.h"
#include "BlobData.h"

CBlobData::CBlobData(int width, int height)
{
	m_Width = width;
	m_Height = height;
	m_Boundary = new vector<int>();
	m_Pixels = new vector<int>();
	m_Intensities = new vector<unsigned short>();
}

CBlobData::CBlobData(CBlobData *blob)
{
	m_Width = blob->m_Width;
	m_Height = blob->m_Height;
	m_Boundary = new vector<int>();
	m_Pixels = new vector<int>();
	m_Intensities = new vector<unsigned short>();
	for (int i = 0; i < (int)m_Pixels->size(); i++)
	{
		m_Pixels->push_back((*blob->m_Pixels)[i]);
		m_Intensities->push_back((*blob->m_Intensities)[i]);
	}
}

CBlobData::~CBlobData()
{
	m_Boundary->clear();
	delete m_Boundary;
	m_Pixels->clear();
	delete m_Pixels;
	m_Intensities->clear();
	delete m_Intensities;
}

