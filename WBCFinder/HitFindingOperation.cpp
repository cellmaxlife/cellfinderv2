#include "stdafx.h"
#include "HitFindingOperation.h"
#include <string.h>
#include "CTCParams.h"
#include "VersionNumber.h"
#include <math.h>
#include <thread>

#define MIN_ARTIFACT_PIXEL_COUNT 1000
#define MAX_BLOB_PIXEL_COUNT		1500
#define THRESHOLD_INCREMENT_STEP	50

CHitFindingOperation::CHitFindingOperation()
{
}

CHitFindingOperation::~CHitFindingOperation()
{
}

void CHitFindingOperation::FreeBlobList(vector<CBlobData *> *blobList)
{
	for (int i = 0; i < (int)blobList->size(); i++)
	{
		if ((*blobList)[i] != NULL)
		{
			delete (*blobList)[i];
			(*blobList)[i] = NULL;
		}
	}
	blobList->clear();
}

void CHitFindingOperation::FreeBlobTree(vector<CBlobTreeNode *> *blobTree)
{
	for (int i = 0; i < (int)blobTree->size(); i++)
	{
		if ((*blobTree)[i] != NULL)
		{
			delete (*blobTree)[i];
			(*blobTree)[i] = NULL;
		}
	}
	blobTree->clear();
}

void CHitFindingOperation::FreeRGNList(vector<CRGNData *> *rgnList)
{
	for (int i = 0; i < (int)rgnList->size(); i++)
	{
		if ((*rgnList)[i] != NULL)
		{
			delete (*rgnList)[i];
			(*rgnList)[i] = NULL;
		}
	}
	rgnList->clear();
}

void CHitFindingOperation::ErosionOperation(unsigned char *inImage, unsigned char *outImage, int width, int height)
{
	memset(outImage, 0, sizeof(unsigned char) * width * height);

	for (int i = 1; i < (height - 1); i++)
	{
		for (int j = 1; j < (width - 1); j++)
		{
			bool keep = true;
			for (int k = -1; k < 2; k++)
			{
				for (int h = -1; h < 2; h++)
				{
					if (inImage[width * (i + k) + (j + h)] != (unsigned char)255)
					{
						keep = false;
						break;
					}
				}
				if (!keep)
					break;
			}
			if (keep)
				outImage[width * i + j] = (unsigned char)255;
		}
	}
}

void CHitFindingOperation::GetBlobBoundary(CBlobData *blob)
{
	unsigned char *temp1 = new unsigned char[blob->m_Width * blob->m_Height];
	unsigned char *temp2 = new unsigned char[blob->m_Width * blob->m_Height];
	memset(temp2, 0, sizeof(unsigned char) * blob->m_Width * blob->m_Height);
	for (int i = 0; i < (int)blob->m_Pixels->size(); i++)
	{
		int x = (int)((*blob->m_Pixels)[i] % blob->m_Width);
		int y = (int)((*blob->m_Pixels)[i] / blob->m_Width);
		temp2[blob->m_Width * y + x] = (unsigned char)255;
	}
	ErosionOperation(temp2, temp1, blob->m_Width, blob->m_Height);
	blob->m_Boundary->clear();
	for (int i = 0; i < blob->m_Height; i++)
	{
		for (int j = 0; j < blob->m_Width; j++)
		{
			if ((temp2[blob->m_Width * i + j] == (unsigned char)255) &&
				(temp1[blob->m_Width * i + j] == (unsigned char)0))
			{
				int pos = (unsigned int) (i * blob->m_Width + j);
				blob->m_Boundary->push_back(pos);
			}
		}
	}
	delete[] temp1;
	delete[] temp2;
}

void CHitFindingOperation::ScanImage(CCTCParams *params, CSingleChannelTIFFData *redImage, CSingleChannelTIFFData *greenImage,
	CSingleChannelTIFFData *blueImage, vector<CRGNData *> *outputList, bool isBatchMode)
{
	CString message;
	for (int i = 0; i < (int)PARAM_LASTONE; i++)
	{
		message.Format(_T("%s=%d"), params->m_CTCParamNames[i], params->m_CTCParams[i]);
		m_Log->Message(message);
	}
	for (int i = 1; i < ((int)PARAM_FLOAT_LAST) - ((int)PARAM_FLOAT_FIRST); i++)
	{
		message.Format(_T("%s=%.1f"), params->m_GroupNames[i], params->m_GroupParams[i]);
		m_Log->Message(message);
	}

	CleanUpExclusionArea(redImage, greenImage, blueImage);

	int regionWidth = SAMPLEFRAMEWIDTH;
	int regionHeight = SAMPLEFRAMEHEIGHT;

	int NumColumns = redImage->GetImageWidth() / regionWidth;
	int NumRows = redImage->GetImageHeight() / regionHeight;

	message.Format(_T("NumRows=%d,NumColumns=%d"), NumRows, NumColumns);
	m_Log->Message(message);

	unsigned short *redRegionImage = new unsigned short[regionWidth * regionHeight];
	unsigned short *greenRegionImage = new unsigned short[regionWidth * regionHeight];
	unsigned short *blueRegionImage = new unsigned short[regionWidth * regionHeight];

	vector<CRGNData *> cellList;
	CRGNData *region = new CRGNData(0, 0, regionWidth, regionHeight);
	region->SetCPI(RED_COLOR, redImage->GetCPI());
	region->SetCPI(GREEN_COLOR, greenImage->GetCPI());
	region->SetCPI(BLUE_COLOR, blueImage->GetCPI());
	for (int i = 0; i < NumRows; i++)
	{
		for (int j = 0; j < NumColumns; j++)
		{
			int x0 = regionWidth * j;
			int y0 = regionHeight * i;
			bool retRed = redImage->GetImageRegion(x0, y0, regionWidth, regionHeight, redRegionImage);
			bool retGreen = greenImage->GetImageRegion(x0, y0, regionWidth, regionHeight, greenRegionImage);
			bool retBlue = blueImage->GetImageRegion(x0, y0, regionWidth, regionHeight, blueRegionImage);
			if (retRed && retGreen && retBlue)
			{
				region->SetPosition(x0, y0);
				region->SetImages(redRegionImage, greenRegionImage, blueRegionImage);
				ProcessOnePatch(params, region, &cellList);
				region->NullImages();
			}
		}
	}

	for (int i = 0; i < (int)cellList.size(); i++)
	{
		int xCenter, yCenter;
		bool ret = GetCellCenterLocation(cellList[i], &xCenter, &yCenter);
		if (ret)
		{
			int xPos, yPos;
			cellList[i]->GetPosition(&xPos, &yPos);
			int x0 = xPos + xCenter - (SAMPLEFRAMEWIDTH / 2);
			int y0 = yPos + yCenter - (SAMPLEFRAMEHEIGHT / 2);
			bool retRed = redImage->GetImageRegion(x0, y0, regionWidth, regionHeight, redRegionImage);
			bool retGreen = greenImage->GetImageRegion(x0, y0, regionWidth, regionHeight, greenRegionImage);
			bool retBlue = blueImage->GetImageRegion(x0, y0, regionWidth, regionHeight, blueRegionImage);
			if (retRed && retGreen && retBlue)
			{
				region->SetPosition(x0, y0);
				region->SetImages(redRegionImage, greenRegionImage, blueRegionImage);
				vector<CRGNData *> cellList1;
				ProcessOnePatch(params, region, &cellList1);
				region->NullImages();
				if (PickARegionAtCenter(region, &cellList1))
				{
					CRGNData *newRegion = new CRGNData(region);
					outputList->push_back(newRegion);
				}
				FreeRGNList(&cellList1);
			}
		}
	}
	FreeRGNList(&cellList);
	delete region;
	delete[] redRegionImage;
	delete[] greenRegionImage;
	delete[] blueRegionImage;

	for (int i = 0; i < (int)outputList->size(); i++)
	{
		cellList.push_back((*outputList)[i]);
		(*outputList)[i] = NULL;
	}
	FreeRGNList(outputList);

	message.Format(_T("#Cells found before sorting =%u"), cellList.size());
	m_Log->Message(message);

	QuickSort(&cellList, 0, (int)cellList.size() - 1);

	message.Format(_T("Sorting Done"));
	m_Log->Message(message);

	for (int i = 0; i < (int)cellList.size() - 1; i++)
	{
		if (cellList[i] != NULL)
		{
			int xi, yi;
			cellList[i]->GetPosition(&xi, &yi);
			for (int j = i + 1; j < (int)cellList.size(); j++)
			{
				if (cellList[j] != NULL)
				{
					int xj, yj;
					cellList[j]->GetPosition(&xj, &yj);
					if ((abs(xi - xj) < 5) && (abs(yi - yj) < 5))
					{
						delete cellList[j];
						cellList[j] = NULL;
					}
				}
			}
		}
	}

	for (int i = (int)cellList.size() - 1; i >= 0; i--)
	{
		if (cellList[i] != NULL)
		{
			CRGNData *ptr = cellList[i];
			outputList->push_back(ptr);
			cellList[i] = NULL;
		}
	}

	cellList.clear();

	message.Format(_T("done with Coping to outputList, #cells=%u"), outputList->size());
	m_Log->Message(message);
}

int CHitFindingOperation::GetOverlapPixelCount(vector<int> *blob1, vector<int> *blob2, vector<int> *blob3)
{
	int count = 0;
	int index1 = 0;
	int index2 = 0;
	while ((index1 < (int)blob1->size()) && (index2 < (int)blob2->size()))
	{
		if ((*blob1)[index1] < (*blob2)[index2])
			index1++;
		else if ((*blob1)[index1] > (*blob2)[index2])
		{
			blob3->push_back((*blob2)[index2]);
			index2++;
		}
		else
		{
			count++;
			index1++;
			index2++;
		}
	}
	return count;
}

bool CHitFindingOperation::blobIncluded(vector<int> *list, int blobIndex)
{
	bool ret = false;
	for (int i = 0; i < (int)list->size(); i++)
	{
		if (blobIndex == (*list)[i])
		{
			ret = true;
			break;
		}
	}

	return ret;
}

template<typename T>
void CHitFindingOperation::QuickSort(vector<T> *list, int lo, int hi)
{
	if (lo < hi)
	{
		int position = Partition(list, lo, hi);
		QuickSort(list, lo, position - 1);
		QuickSort(list, position + 1, hi);
	}
}

int CHitFindingOperation::Partition(vector<int> *list, int lo, int hi)
{
	int position = ChoosePivot(list, lo, hi);
	int pos = (*list)[position];
	Swap(list, position, hi);
	int storeIndex = lo;
	for (int i = lo; i < hi; i++)
	{
		if ((*list)[i] < pos)
		{
			Swap(list, i, storeIndex);
			storeIndex++;
		}
	}
	Swap(list, storeIndex, hi);
	return storeIndex;
}

int CHitFindingOperation::ChoosePivot(vector<int> *list, int lo, int hi)
{
	int pivot = lo;
	int pos0 = (*list)[lo];
	int pos1 = (*list)[(lo + hi) / 2];
	int pos2 = (*list)[hi];
	if ((pos0 <= pos1) && (pos1 <= pos2))
		pivot = (lo + hi) / 2;
	else if ((pos0 <= pos2) && (pos2 <= pos1))
		pivot = hi;
	else
		pivot = lo;
	return pivot;
}

int CHitFindingOperation::Partition(vector<CRGNData *> *list, int lo, int hi)
{
	int position = ChoosePivot(list, lo, hi);
	double pos = (*list)[position]->m_Ratio1;
	Swap(list, position, hi);
	int storeIndex = lo;
	for (int i = lo; i < hi; i++)
	{
		if ((*list)[i]->m_Ratio1 < pos)
		{
			Swap(list, i, storeIndex);
			storeIndex++;
		}
	}
	Swap(list, storeIndex, hi);
	return storeIndex;
}

int CHitFindingOperation::ChoosePivot(vector<CRGNData *> *list, int lo, int hi)
{
	int pivot = lo;
	double pos0 = (*list)[lo]->m_Ratio1;
	double pos1 = (*list)[(lo + hi) / 2]->m_Ratio1;
	double pos2 = (*list)[hi]->m_Ratio1;
	if ((pos0 <= pos1) && (pos1 <= pos2))
		pivot = (lo + hi) / 2;
	else if ((pos0 <= pos2) && (pos2 <= pos1))
		pivot = hi;
	else
		pivot = lo;
	return pivot;
}

template<typename T>
void CHitFindingOperation::Swap(vector<T> *list, int index1, int index2)
{
	T ptr = (*list)[index1];
	(*list)[index1] = (*list)[index2];
	(*list)[index2] = ptr;
}

void CHitFindingOperation::CleanUpExclusionArea(CSingleChannelTIFFData *redImg, CSingleChannelTIFFData *greenImg, CSingleChannelTIFFData *blueImg)
{
	const int ARRAYSIZE = 3000;

	int width = redImg->GetImageWidth();
	int height = redImg->GetImageHeight();

	RECT rect[12]; // 0: Left, 1: Right, 2:Top, 3:Bottom
	int width1;
	int height1;

	rect[0].left = 0;
	rect[0].right = ARRAYSIZE;
	rect[0].top = height / 2 - 25;
	rect[0].bottom = height / 2 + 25;

	rect[1].left = 0;
	rect[1].right = ARRAYSIZE;
	rect[1].top = height / 2 - 150;
	rect[1].bottom = height / 2 - 100;

	rect[2].left = 0;
	rect[2].right = ARRAYSIZE;
	rect[2].top = height / 2 + 100;
	rect[2].bottom = height / 2 + 150;

	rect[3].left = width - ARRAYSIZE;
	rect[3].right = width;
	rect[3].top = height / 2 - 25;
	rect[3].bottom = height / 2 + 25;

	rect[4].left = width - ARRAYSIZE;
	rect[4].right = width;
	rect[4].top = height / 2 - 150;
	rect[4].bottom = height / 2 - 100;

	rect[5].left = width - ARRAYSIZE;
	rect[5].right = width;
	rect[5].top = height / 2 + 100;
	rect[5].bottom = height / 2 + 150;

	rect[6].left = width / 2 - 25;
	rect[6].right = width / 2 + 25;
	rect[6].top = 0;
	rect[6].bottom = ARRAYSIZE;

	rect[7].left = width / 2 - 150;
	rect[7].right = width / 2 - 100;
	rect[7].top = 0;
	rect[7].bottom = ARRAYSIZE;

	rect[8].left = width / 2 + 100;
	rect[8].right = width / 2 + 150;
	rect[8].top = 0;
	rect[8].bottom = ARRAYSIZE;

	rect[9].left = width / 2 - 25;
	rect[9].right = width / 2 + 25;
	rect[9].top = height - ARRAYSIZE;
	rect[9].bottom = height;

	rect[10].left = width / 2 - 150;
	rect[10].right = width / 2 - 100;
	rect[10].top = height - ARRAYSIZE;
	rect[10].bottom = height;

	rect[11].left = width / 2 + 100;
	rect[11].right = width / 2 + 150;
	rect[11].top = height - ARRAYSIZE;
	rect[11].bottom = height;

	int left0 = 0;
	int right0 = 0;
	int top0 = 0;
	int bottom0 = 0;

	width1 = rect[0].right - rect[0].left;
	height1 = rect[0].bottom - rect[0].top;
	unsigned short *image = new unsigned short[width1 * height1];
	unsigned short *sumImage = new unsigned short[width1 * height1];
	int *profile[3];
	int size = sizeof(int) * ARRAYSIZE;
	for (int i = 0; i < 3; i++)
	{
		profile[i] = new int[ARRAYSIZE];
		memset(sumImage, 0, sizeof(unsigned short) * width1 * height1);
		redImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		greenImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		blueImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		memset(profile[i], 0, size);
		ImageProjection(sumImage, profile[i], width1, height1, true);
		LowpassFiltering(profile[i], ARRAYSIZE);
	}
	left0 = GetStepEdgePosition(profile, ARRAYSIZE, false) + 300;

	for (int i = 3; i < 6; i++)
	{
		memset(sumImage, 0, sizeof(unsigned short) * width1 * height1);
		redImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		greenImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		blueImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		memset(profile[i - 3], 0, size);
		ImageProjection(sumImage, profile[i - 3], width1, height1, true);
		LowpassFiltering(profile[i - 3], ARRAYSIZE);
	}

	right0 = rect[3].left + GetStepEdgePosition(profile, ARRAYSIZE, true) - 300;
	delete[] image;
	delete[] sumImage;

	width1 = rect[6].right - rect[6].left;
	height1 = rect[6].bottom - rect[6].top;
	image = new unsigned short[width1 * height1];
	sumImage = new unsigned short[width1 * height1];

	for (int i = 6; i < 9; i++)
	{
		memset(sumImage, 0, sizeof(unsigned short) * width1 * height1);
		redImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		greenImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		blueImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		memset(profile[i - 6], 0, size);
		ImageProjection(sumImage, profile[i - 6], width1, height1, false);
		LowpassFiltering(profile[i - 6], ARRAYSIZE);
	}

	top0 = GetStepEdgePosition(profile, ARRAYSIZE, false) + 300;

	for (int i = 9; i < 12; i++)
	{
		memset(sumImage, 0, sizeof(unsigned short) * width1 * height1);
		redImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		greenImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		blueImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		memset(profile[i - 9], 0, size);
		ImageProjection(sumImage, profile[i - 9], width1, height1, false);
		LowpassFiltering(profile[i - 9], ARRAYSIZE);
	}

	bottom0 = rect[9].top + GetStepEdgePosition(profile, ARRAYSIZE, true) - 300;

	delete[] image;
	delete[] sumImage;
	for (int i = 0; i < 3; i++)
		delete[] profile[i];

	int centerX = (right0 + left0) / 2;
	int centerY = (top0 + bottom0) / 2;
	int radiusX = centerX - left0;
	int radiusY = centerY - top0;
	CString message;
	message.Format(_T("Exclusion: CenterX=%d, CenterY=%d, RadiusX=%d, RadiusY=%d"), centerX, centerY, radiusX, radiusY);
	m_Log->Message(message);
	message.Format(_T("Exclusion: Left0=%d, Top0=%d, Right0=%d, Bottom0=%d"), left0, top0, right0, bottom0);
	m_Log->Message(message);
	redImg->CleanUpExclusionArea(centerX, centerY, radiusX, radiusY);
	greenImg->CleanUpExclusionArea(centerX, centerY, radiusX, radiusY);
	blueImg->CleanUpExclusionArea(centerX, centerY, radiusX, radiusY);
}

void CHitFindingOperation::SumUpImageData(unsigned short *image, unsigned short *sumImage, int width, int height)
{
	unsigned short *ptrImage = image;
	unsigned short *ptrSum = sumImage;
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++, ptrImage++, ptrSum++)
		{
			*ptrSum += *ptrImage;
		}
	}
}

void CHitFindingOperation::ImageProjection(unsigned short *sumImage, int *profile, int width, int height, bool verticalProjection)
{
	if (verticalProjection)
	{
		for (int i = 0; i < width; i++)
		{
			for (int j = 0; j < height; j++)
			{
				profile[i] += *(sumImage + width * j + i);
			}
		}
	}
	else
	{
		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++)
			{
				profile[i] += *(sumImage + width * i + j);
			}
		}
	}
}

int CHitFindingOperation::GetStepEdgePosition(int *profile[], int length, bool startFromBeginning)
{
	int stepsize = 10;
	int length1 = length / stepsize;
	int *temp = new int[length1];
	memset(temp, 0, sizeof(int)* length1);
	if (startFromBeginning)
	{
		for (int i = 3; i < (length - 6); i++)
		{
			for (int j = 0; j < 3; j++)
			{
				temp[i / stepsize] += (*(profile[j] + i + 1) - *(profile[j] + i));
			}
		}
	}
	else
	{
		for (int i = (length - 6); i > 3; i--)
		{
			for (int j = 0; j < 3; j++)
			{
				temp[i / stepsize] += (*(profile[j] + i - 1) - *(profile[j] + i));
			}
		}
	}
	int max = 0;
	int maxi = 0;
	for (int i = 0; i < length1; i++)
	{
		if (temp[i] > max)
		{
			max = temp[i];
			maxi = i;
		}
	}
	delete[] temp;
	return (stepsize * maxi);
}

void CHitFindingOperation::LowpassFiltering(int *profile, int length)
{
	int *temp = new int[length - 8];
	temp[0] = 0;

	for (int i = 0; i < 7; i++)
		temp[0] += profile[i];

	for (int i = 1; i < (length - 8); i++)
		temp[i] = temp[i - 1] - profile[i] + profile[i + 7];

	for (int i = 0; i < 3; i++)
		profile[i] = 0;

	for (int i = 3; i < (length - 5); i++)
		profile[i] = temp[i - 3] / 7;

	for (int i = (length - 5); i < length; i++)
		profile[i] = 0;

	delete temp;
}

int CHitFindingOperation::GetAverageIntensity(CRGNData *region, PIXEL_COLOR_TYPE color)
{
	int IntensitySum = 0;
	int PixelCount = 0;
	int maxIntensity = 255;
	for (int i = 0; i < (int)region->GetBlobData(color)->size(); i++)
	{
		PixelCount += (int)(*region->GetBlobData(color))[i]->m_Pixels->size();
		for (int j = 0; j < (int)(*region->GetBlobData(color))[i]->m_Pixels->size(); j++)
		{
			int intensity = (int)(*(*region->GetBlobData(color))[i]->m_Intensities)[j];
			IntensitySum += intensity;
			if (intensity > maxIntensity)
				maxIntensity = intensity;
		}
	}
	int Average = 0;
	if (PixelCount > 0)
		Average = IntensitySum / PixelCount;
	if (color == RED_COLOR)
		region->m_RedFrameMax = maxIntensity;
	else if (color == GREEN_COLOR)
		region->m_GreenFrameMax = maxIntensity;
	else
		region->m_BlueAverage = maxIntensity;
	return Average;
}

void CHitFindingOperation::CalculateBoundary(vector<CBlobData *> *blobData)
{
	for (int i = 0; i < (int)blobData->size(); i++)
	{
		GetBlobBoundary((*blobData)[i]);
	}
}

bool CHitFindingOperation::ProcessOneRegion(CCTCParams *params, CRGNData *region, bool isZeissData)
{
	vector<CRGNData *> regionList;
	bool ret = ProcessOnePatch(params, region, &regionList);
	if (ret)
	{
		int maxIndex = -1;
		int maxRedIntensitySum = 0;
		for (int i = 0; i < (int)regionList.size(); i++)
		{
			int redIntensitySum = regionList[i]->GetPixels(RED_COLOR) * regionList[i]->m_RedValue;
			if (redIntensitySum > maxRedIntensitySum)
			{
				maxRedIntensitySum = redIntensitySum;
				maxIndex = i;
			}
		}
		if (maxIndex > -1)
		{
			region->CopyRegionData(regionList[maxIndex]);
			CalculateBoundary(region->GetBlobData(RED_COLOR));
			CalculateBoundary(region->GetBlobData(GREEN_COLOR));
			CalculateBoundary(region->GetBlobData(BLUE_COLOR));
			CalculateBoundary(region->GetBlobData(RB_COLORS));
		}
	}
	FreeRGNList(&regionList);
	return ret;
}

bool CHitFindingOperation::MergeBlobsToGetCTC(CCTCParams *params, CRGNData *region, vector<CRGNData *> *rgnList)
{
	vector<CRGNData *> hitList;
	vector<int> redBlobs;
	vector<int> greenBlobs;
	vector<int> blueBlobs;
	for (int i = 0; i < (int)region->GetBlobData(RED_COLOR)->size(); i++)
	{
		CBlobData *redBlob = (*region->GetBlobData(RED_COLOR))[i];
		if (redBlob == NULL)
			continue;
		redBlobs.clear();
		greenBlobs.clear();
		blueBlobs.clear();
		int OverlappedRedBluePixelCount = 0;
		redBlobs.push_back(i);
		vector<int> RBBlobPixels;
		for (int j = 0; j < (int)redBlob->m_Pixels->size(); j++)
			RBBlobPixels.push_back((*redBlob->m_Pixels)[j]);
		bool newPixelAdded = true;
		while (newPixelAdded)
		{
			newPixelAdded = false;
			for (int j = 0; j < (int)region->GetBlobData(BLUE_COLOR)->size(); j++)
			{
				CBlobData *blueBlob = (*region->GetBlobData(BLUE_COLOR))[j];
				if (blueBlob == NULL)
					continue;
				if (blobIncluded(&blueBlobs, j))
					continue;
				vector<int> notOverlappedBluePixels;
				int overlappedPixels = GetOverlapPixelCount(&RBBlobPixels, blueBlob->m_Pixels, &notOverlappedBluePixels);
				if (overlappedPixels > 0)
				{
					newPixelAdded = true;
					OverlappedRedBluePixelCount += overlappedPixels;
					blueBlobs.push_back(j);
					for (int k = 0; k < (int)notOverlappedBluePixels.size(); k++)
						RBBlobPixels.push_back(notOverlappedBluePixels[k]);
					QuickSort(&RBBlobPixels, 0, (int)RBBlobPixels.size() - 1);
				}
				notOverlappedBluePixels.clear();
			}
			if (newPixelAdded)
			{
				newPixelAdded = false;
				for (int j = 0; j < (int)region->GetBlobData(RED_COLOR)->size(); j++)
				{
					CBlobData *redBlob1 = (*region->GetBlobData(RED_COLOR))[j];
					if (redBlob1 == NULL)
						continue;
					if (blobIncluded(&redBlobs, j))
						continue;
					vector<int> notOverlappedRedPixels;
					int overlappedPixels = GetOverlapPixelCount(&RBBlobPixels, redBlob1->m_Pixels, &notOverlappedRedPixels);
					if (overlappedPixels > 0)
					{
						newPixelAdded = true;
						OverlappedRedBluePixelCount += overlappedPixels;
						redBlobs.push_back(j);
						for (int k = 0; k < (int)notOverlappedRedPixels.size(); k++)
							RBBlobPixels.push_back(notOverlappedRedPixels[k]);
						QuickSort(&RBBlobPixels, 0, (int)RBBlobPixels.size() - 1);
					}
					notOverlappedRedPixels.clear();
				}
			}
		}
		if (OverlappedRedBluePixelCount > 0)
		{
			int x0, y0;
			region->GetPosition(&x0, &y0);
			CRGNData *newHit = new CRGNData(x0, y0, region->GetWidth(), region->GetHeight());
			newHit->SetCPI(RED_COLOR, region->GetCPI(RED_COLOR));
			newHit->SetCPI(GREEN_COLOR, region->GetCPI(GREEN_COLOR));
			newHit->SetCPI(BLUE_COLOR, region->GetCPI(BLUE_COLOR));
			hitList.push_back(newHit);
			newHit->m_RedBlueOverlaps = OverlappedRedBluePixelCount;
			CBlobData *rbBlob = new CBlobData(region->GetWidth(), region->GetHeight());
			newHit->GetBlobData(RB_COLORS)->push_back(rbBlob);
			for (int j = 0; j < (int)RBBlobPixels.size(); j++)
				rbBlob->m_Pixels->push_back(RBBlobPixels[j]);
			for (int j = 0; j < (int)region->GetBlobData(RED_COLOR)->size(); j++)
			{
				CBlobData *redBlob1 = (*region->GetBlobData(RED_COLOR))[j];
				if ((redBlob1 != NULL) && (blobIncluded(&redBlobs, j)))
				{
					newHit->GetBlobData(RED_COLOR)->push_back(redBlob1);
					(*region->GetBlobData(RED_COLOR))[j] = NULL;
				}
			}
			for (int j = 0; j < (int)region->GetBlobData(BLUE_COLOR)->size(); j++)
			{
				CBlobData *blueBlob1 = (*region->GetBlobData(BLUE_COLOR))[j];
				if ((blueBlob1 != NULL) && (blobIncluded(&blueBlobs, j)))
				{
					newHit->GetBlobData(BLUE_COLOR)->push_back(blueBlob1);
					(*region->GetBlobData(BLUE_COLOR))[j] = NULL;
				}
			}
			for (int j = 0; j < (int)region->GetBlobData(GREEN_COLOR)->size(); j++)
			{
				CBlobData *greenBlob1 = (*region->GetBlobData(GREEN_COLOR))[j];
				if (greenBlob1 == NULL)
					continue;
				vector<int> notOverlappedGreenPixels;
				int overlappedPixels = GetOverlapPixelCount(&RBBlobPixels, greenBlob1->m_Pixels, &notOverlappedGreenPixels);
				if (overlappedPixels > 0)
				{
					newHit->m_GreenBlueOverlaps += overlappedPixels;
					newHit->GetBlobData(GREEN_COLOR)->push_back(greenBlob1);
					(*region->GetBlobData(GREEN_COLOR))[j] = NULL;
				}
				notOverlappedGreenPixels.clear();
			}
			RBBlobPixels.clear();
		}
	}
	for (int i = 0; i < (int)hitList.size(); i++)
	{
		CalculateHitAttributes(params, hitList[i]);
		rgnList->push_back(hitList[i]);
		hitList[i] = NULL;
	}
	bool ret = false;
	if (hitList.size() > 0)
		ret = true;
	FreeRGNList(&hitList);
	return ret;
}

int CHitFindingOperation::GetPixelCountSum(CRGNData *region, PIXEL_COLOR_TYPE color)
{
	int pixelCount = 0;
	
	for (int i = 0; i < (int)region->GetBlobData(color)->size(); i++)
		pixelCount += (int)(*region->GetBlobData(color))[i]->m_Pixels->size();
	
	return pixelCount;
}

void CHitFindingOperation::ScanSingleFrame(CCTCParams *params, CSingleChannelTIFFData *redImage, CSingleChannelTIFFData *greenImage,
	CSingleChannelTIFFData *blueImage, vector<CRGNData *> *outputList, int FrameX0, int FrameY0)
{
	CString message;

	int regionWidth = SAMPLEFRAMEWIDTH;
	int regionHeight = SAMPLEFRAMEHEIGHT;
	
	int NumColumns = CAMERA_FRAME_WIDTH / regionWidth;
	int NumRows = CAMERA_FRAME_HEIGHT / regionHeight;

	message.Format(_T("FrameX0=%d,FrameY0=%d,NumRows=%d,NumColumns=%d"), FrameX0, FrameY0, NumRows, NumColumns);
	m_Log->Message(message);

	unsigned short *redRegionImage = new unsigned short[regionWidth * regionHeight];
	unsigned short *greenRegionImage = new unsigned short[regionWidth * regionHeight];
	unsigned short *blueRegionImage = new unsigned short[regionWidth * regionHeight];
	CRGNData *region = new CRGNData(0, 0, regionWidth, regionHeight);
	region->SetCPI(RED_COLOR, redImage->GetCPI());
	region->SetCPI(GREEN_COLOR, greenImage->GetCPI());
	region->SetCPI(BLUE_COLOR, blueImage->GetCPI());
	vector<CRGNData *> cellList;
	for (int i = 0; i < NumRows; i++)
	{
		for (int j = 0; j < NumColumns; j++)
		{
			int x0 = FrameX0 + regionWidth * j;
			int y0 = FrameY0 + regionHeight * i;
			bool retRed = redImage->GetImageRegion(x0, y0, regionWidth, regionHeight, redRegionImage);
			bool retGreen = greenImage->GetImageRegion(x0, y0, regionWidth, regionHeight, greenRegionImage);
			bool retBlue = blueImage->GetImageRegion(x0, y0, regionWidth, regionHeight, blueRegionImage);
			if (retRed && retGreen && retBlue)
			{
				region->SetPosition(x0, y0);
				region->SetImages(redRegionImage, greenRegionImage, blueRegionImage);
				ProcessOnePatch(params, region, &cellList);
				region->NullImages();
			}
		}
	}
	
	for (int i = 0; i < (int)cellList.size(); i++)
	{
		int xCenter, yCenter;
		bool ret = GetCellCenterLocation(cellList[i], &xCenter, &yCenter);
		if (ret)
		{
			int xPos, yPos;
			cellList[i]->GetPosition(&xPos, &yPos);
			int x0 = xPos + xCenter - (SAMPLEFRAMEWIDTH / 2);
			int y0 = yPos + yCenter - (SAMPLEFRAMEHEIGHT / 2);
			bool retRed = redImage->GetImageRegion(x0, y0, regionWidth, regionHeight, redRegionImage);
			bool retGreen = greenImage->GetImageRegion(x0, y0, regionWidth, regionHeight, greenRegionImage);
			bool retBlue = blueImage->GetImageRegion(x0, y0, regionWidth, regionHeight, blueRegionImage);
			if (retRed && retGreen && retBlue)
			{
				region->SetPosition(x0, y0);
				region->SetImages(redRegionImage, greenRegionImage, blueRegionImage);
				vector<CRGNData *> cellList1;
				ProcessOnePatch(params, region, &cellList1);
				region->NullImages();
				if (PickARegionAtCenter(region, &cellList1))
				{
					CRGNData *newRegion = new CRGNData(region);
					outputList->push_back(newRegion);
				}
				FreeRGNList(&cellList1);
			}
		}
	}
	FreeRGNList(&cellList);
	delete region;
	delete[] redRegionImage;
	delete[] greenRegionImage;
	delete[] blueRegionImage;

	message.Format(_T("#Cells found so far = %u"), outputList->size());
	m_Log->Message(message);
}

bool CHitFindingOperation::PickARegionAtCenter(CRGNData *region, vector<CRGNData *> *regionList)
{
	bool ret = false;
	int minDistance = MAXINT;
	int minIndex = -1;
	for (int i = 0; i < (int)regionList->size(); i++)
	{
		int xCenter, yCenter;
		if (GetCellCenterLocation((*regionList)[i], &xCenter, &yCenter))
		{
			int dx = xCenter - (SAMPLEFRAMEWIDTH / 2);
			int dy = yCenter - (SAMPLEFRAMEHEIGHT / 2);
			int distance = (int)sqrt((double)(dx * dx) + (double)(dy * dy));
			if (distance < minDistance)
			{
				minDistance = distance;
				minIndex = i;
			}
		}
	}
	if (minIndex > -1)
	{
		ret = true;
		region->CopyRegionData((*regionList)[minIndex]);
		CalculateBoundary(region->GetBlobData(RED_COLOR));
		CalculateBoundary(region->GetBlobData(GREEN_COLOR));
		CalculateBoundary(region->GetBlobData(BLUE_COLOR));
		CalculateBoundary(region->GetBlobData(RB_COLORS));
	}
	return ret;
}

bool CHitFindingOperation::GetCellCenterLocation(CRGNData *region, int *xCenter, int *yCenter)
{
	bool ret = false;
	
	if ((region->GetBlobData(RB_COLORS)->size() > 0) && ((*region->GetBlobData(RB_COLORS))[0] != NULL)
		&& ((*region->GetBlobData(RB_COLORS))[0]->m_Pixels != NULL) 
		&& ((*region->GetBlobData(RB_COLORS))[0]->m_Pixels->size() > 0))
	{
		vector<int> *pixels = (*region->GetBlobData(RB_COLORS))[0]->m_Pixels;
		RECT rect;
		rect.left = (*region->GetBlobData(RB_COLORS))[0]->m_Width;
		rect.top = (*region->GetBlobData(RB_COLORS))[0]->m_Height;
		rect.right = 0;
		rect.bottom = 0;
		int width = rect.left;
		for (int i = 0; i < (int)pixels->size(); i++)
		{
			int position = (*pixels)[i];
			int x0 = position % width;
			int y0 = position / width;
			if (x0 < rect.left)
				rect.left = x0;
			if (x0 > rect.right)
				rect.right = x0;
			if (y0 < rect.top)
				rect.top = y0;
			if (y0 > rect.bottom)
				rect.bottom = y0;
		}
		*xCenter = (rect.left + rect.right) / 2;
		*yCenter = (rect.top + rect.bottom) / 2;
		ret = true;
	}

	return ret;
}

void CHitFindingOperation::SortRegionList(vector<CRGNData *> *outputList)
{
	QuickSort(outputList, 0, (int)outputList->size() - 1);

	for (int i = 0; i < (int)(outputList->size() / 2); i++)
	{
		CRGNData *ptr = (*outputList)[i];
		(*outputList)[i] = (*outputList)[(int)outputList->size() - 1 - i];
		(*outputList)[(int)outputList->size() - 1 - i] = ptr;
	}
}

void CHitFindingOperation::CalculateHitAttributes(CCTCParams *params, CRGNData *region)
{
	region->SetPixels(RED_COLOR, GetPixelCountSum(region, RED_COLOR));
	region->SetPixels(GREEN_COLOR, GetPixelCountSum(region, GREEN_COLOR));
	region->SetPixels(BLUE_COLOR, GetPixelCountSum(region, BLUE_COLOR));
	region->SetPixels(RB_COLORS, GetPixelCountSum(region, RB_COLORS));
	region->SetPixels(GB_COLORS, GetPixelCountSum(region, GB_COLORS));
	region->m_RedAverage = GetAverageIntensity(region, RED_COLOR);
	region->m_GreenAverage = GetAverageIntensity(region, GREEN_COLOR);
	region->m_BlueAverage = GetAverageIntensity(region, BLUE_COLOR);

	if (region->m_RedAverage > region->GetCPI(RED_COLOR))
		region->m_RedValue = region->m_RedAverage - region->GetCPI(RED_COLOR);
	else
		region->m_RedValue = 0;
	if (region->m_GreenAverage > region->GetCPI(GREEN_COLOR))
		region->m_GreenValue = region->m_GreenAverage - region->GetCPI(GREEN_COLOR);
	else
		region->m_GreenValue = 0;

	float nominator = (float)(params->m_GroupParams[(int)PARAM_NOMINATOR_R_COEF1 - (int)PARAM_FLOAT_FIRST] * region->m_RedValue
		+ params->m_GroupParams[(int)PARAM_NOMINATOR_G_COEF1 - (int)PARAM_FLOAT_FIRST] * region->m_GreenValue);
	float denominator = (float)(params->m_GroupParams[(int)PARAM_DENOMINATOR_R_COEF1 - (int)PARAM_FLOAT_FIRST] * region->m_RedValue
		+ params->m_GroupParams[(int)PARAM_DENOMINATOR_G_COEF1 - (int)PARAM_FLOAT_FIRST] * region->m_GreenValue + 1.0);

	if ((nominator > 0) && (denominator > 0))
		region->m_Ratio1 = ((double)nominator) / ((double)denominator);
	else if (nominator <= 0)
		region->m_Ratio1 = 0;
	else
	{
		region->m_Ratio1 = nominator;
	}

	if (region->m_Ratio1 <= (double)params->m_GroupParams[(int)PARAM_COEF1_THRESHOLD - (int)PARAM_FLOAT_FIRST])
	{
		region->SetColorCode(NONCTC);
	}
	else
	{
		region->SetColorCode(CTC);
	}
}

void CHitFindingOperation::FindBlobs(unsigned short *image, vector<CBlobData *> *blobs, int *CPI)
{
	int IntensitySum = 0;
	int PixelCount = 0;
	vector<INTENSITY_PIXEL_SET *> pixelIntensityList;
	SortPixelIntensities(image, &pixelIntensityList);
	if (pixelIntensityList.size() > 0)
	{
		int maxCount = 0;
		int maxIndex = -1;
		for (int i = 0; i < (int)pixelIntensityList.size(); i++)
		{
			if ((int)pixelIntensityList[i]->pixelList->size() > maxCount)
			{
				maxIndex = i;
				maxCount = (int)pixelIntensityList[i]->pixelList->size();
			}
		}
		if (maxCount > MIN_ARTIFACT_PIXEL_COUNT)
		{
			for (int i = 0; i < (int)pixelIntensityList[maxIndex]->pixelList->size(); i++)
			{
				image[(*pixelIntensityList[maxIndex]->pixelList)[i]] = (unsigned short)0;
			}
			pixelIntensityList[maxIndex]->intensity = 0;
		}
		for (int i = 0; i< (int)pixelIntensityList.size(); i++)
		{
			PixelCount += (int)pixelIntensityList[i]->pixelList->size();
			IntensitySum += (pixelIntensityList[i]->intensity * (int)pixelIntensityList[i]->pixelList->size());
		}
		for (int i = 0; i < (int)pixelIntensityList.size(); i++)
		{
			delete pixelIntensityList[i];
			pixelIntensityList[i] = NULL;
		}
		pixelIntensityList.clear();
	}
	
	if (PixelCount == 0)
		return;

	int threshold = (IntensitySum / PixelCount) + THRESHOLD_INCREMENT_STEP;
	*CPI = threshold;

	CFindConnectedPixels *finder = new CFindConnectedPixels();
	vector<CBlobData *> blobList;

	finder->FindConnectedPixels(image, SAMPLEFRAMEWIDTH, SAMPLEFRAMEHEIGHT, threshold, &blobList);
	delete finder;
	finder = NULL;

	if (blobList.size() > 0)
	{
		vector<CBlobTreeNode *> blobTree;
		for (int i = 0; i < (int)blobList.size(); i++)
		{
			CBlobTreeNode *node = new CBlobTreeNode(blobList[i], threshold);
			blobTree.push_back(node);
			if ((int)blobList[i]->m_Pixels->size() > MAX_BLOB_PIXEL_COUNT)
				FindBlob(node);
			blobList[i] = NULL;
		}
		blobList.clear();
		vector<CBlobTreeNode *> leafNodes;
		PickLeafNodes(&blobTree, &leafNodes);
		for (int i = 0; i < (int)leafNodes.size(); i++)
		{
			if (leafNodes[i]->m_Node->m_Pixels->size() < MAX_BLOB_PIXEL_COUNT)
			{
				blobs->push_back(leafNodes[i]->m_Node);
				leafNodes[i]->m_Node = NULL;
			}
		}
		leafNodes.clear();
		FreeBlobTree(&blobTree);
	}
}

void CHitFindingOperation::PickLeafNodes(vector<CBlobTreeNode *> *treeHead, vector<CBlobTreeNode *> *leafNodes)
{
	for (int i = 0; i < (int)treeHead->size(); i++)
	{
		if (((*treeHead)[i]->m_Children != NULL) && ((*treeHead)[i]->m_Children->size() > 0))
		{
			PickLeafNodes((*treeHead)[i]->m_Children, leafNodes);
		}
		else
		{
			leafNodes->push_back((*treeHead)[i]);
		}
	}
}

void CHitFindingOperation::FindBlob(CBlobTreeNode *node)
{
	CFindConnectedPixels *finder = new CFindConnectedPixels();
	vector<CBlobData *> blobList;
	int threshold = node->m_Threshold + THRESHOLD_INCREMENT_STEP;
	finder->FindConnectedPixels(node->m_Node, threshold, &blobList);
	delete finder;
	finder = NULL;
	if ((int)blobList.size() > 0)
	{
		node->m_Children = new vector<CBlobTreeNode *>();
		for (int i = 0; i < (int)blobList.size(); i++)
		{
			CBlobTreeNode *child = new CBlobTreeNode(blobList[i], threshold);
			node->m_Children->push_back(child);
			if ((int)blobList[i]->m_Pixels->size() > MAX_BLOB_PIXEL_COUNT)
				FindBlob(child);
			blobList[i] = NULL;
		}
		blobList.clear();
	}
}

void CHitFindingOperation::SortPixelIntensities(unsigned short *image, vector<INTENSITY_PIXEL_SET *> *sortedList)
{
	int length = SAMPLEFRAMEWIDTH * SAMPLEFRAMEHEIGHT;
	unsigned short *imgPtr = image;
	for (int i = 0; i < length; i++, imgPtr++)
	{
		int intensity = (int)*imgPtr;
		AddOnePixel(sortedList, *imgPtr, i);
	}
}

void CHitFindingOperation::AddOnePixel(vector<INTENSITY_PIXEL_SET *> *list, unsigned short intensity, int position)
{
	vector<INTENSITY_PIXEL_SET *>::iterator it0;
	for (it0 = list->begin(); it0 != list->end(); it0++)
	{
		if (intensity <= (*it0)->intensity)
			break;
	}
	if (it0 == list->end())
	{
		INTENSITY_PIXEL_SET *newSet = new INTENSITY_PIXEL_SET(intensity, position);
		list->push_back(newSet);
	}
	else if ((*it0)->intensity == intensity)
	{
		(*it0)->pixelList->push_back(position);
	}
	else
	{
		INTENSITY_PIXEL_SET *newSet = new INTENSITY_PIXEL_SET(intensity, position);
		list->insert(it0, newSet);
	}
}

bool CHitFindingOperation::ProcessOnePatch(CCTCParams *params, CRGNData *region, vector<CRGNData *> *rgnList)
{
	FreeBlobList(region->GetBlobData(RED_COLOR));
	FreeBlobList(region->GetBlobData(GREEN_COLOR));
	FreeBlobList(region->GetBlobData(BLUE_COLOR));
	FreeBlobList(region->GetBlobData(RB_COLORS));
	FreeBlobList(region->GetBlobData(GB_COLORS));

	int redCPI = 0;
	int greenCPI = 0;
	int blueCPI = 0;

	std::thread redThread1(FindBlobs, region->GetImage(RED_COLOR), region->GetBlobData(RED_COLOR), &redCPI);
	std::thread greenThread1(FindBlobs, region->GetImage(GREEN_COLOR), region->GetBlobData(GREEN_COLOR), &greenCPI);
	std::thread blueThread1(FindBlobs, region->GetImage(BLUE_COLOR), region->GetBlobData(BLUE_COLOR), &blueCPI);

	redThread1.join();
	greenThread1.join();
	blueThread1.join();

	region->SetCPI(RED_COLOR, redCPI);
	region->SetCPI(GREEN_COLOR, greenCPI);
	region->SetCPI(BLUE_COLOR, blueCPI);

	bool ret = MergeBlobsToGetCTC(params, region, rgnList);

	return ret;
}