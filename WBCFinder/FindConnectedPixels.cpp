#include "stdafx.h"
#include <iostream>
#include "FindConnectedPixels.h"

#define MIN_BLOB_PIXEL_COUNT	10

CFindConnectedPixels::CFindConnectedPixels()
{
	m_Map = NULL;
}

CFindConnectedPixels::~CFindConnectedPixels()
{
	CleanUp();
}

void CFindConnectedPixels::CleanUp()
{
	if (m_Map != NULL)
	{
		delete m_Map;
		m_Map = NULL;
	}
	while (!m_Stack.empty())
	{
		m_Stack.pop();
	}
}

void CFindConnectedPixels::FindConnectedPixels(CBlobData *blob, int threshold, vector<CBlobData *> *blobList)
{
	CleanUp();
	m_Threshold = (unsigned short)threshold;
	m_Width = blob->m_Width;
	m_Height = blob->m_Height;
	m_Image = new unsigned short[m_Width * m_Height];
	memset(m_Image, 0, sizeof(unsigned short) * m_Width * m_Height);
	m_Map = new unsigned short[m_Width * m_Height];
	memset(m_Map, MAXUINT16, sizeof(unsigned short) * m_Width * m_Height);
	for (int i = 0; i < (int)blob->m_Pixels->size(); i++)
	{
		m_Map[(*blob->m_Pixels)[i]] = (unsigned short)0;
		m_Image[(*blob->m_Pixels)[i]] = (*blob->m_Intensities)[i];
	}

	unsigned short currentLabel = (unsigned short)0;

	vector<CBlobData *> blobs;

	for (int i = 0; i < (int)blob->m_Pixels->size(); i++)
	{
		int position = (*blob->m_Pixels)[i];
		if ((m_Map[position] == (unsigned short)0) && (m_Image[position] >= m_Threshold))
		{
			currentLabel++;
			m_Map[position] = currentLabel;
			m_Stack.push(position);
			CBlobData *blob1 = new CBlobData(m_Width, m_Height);
			blob1->m_Pixels->push_back(position);
			blob1->m_Intensities->push_back(m_Image[position]);
			blobs.push_back(blob1);
			DoLabeling(blob1);
		}
	}

	for (int i = 0; i < (int)blobs.size(); i++)
	{
		if ((int)blobs[i]->m_Pixels->size() >= MIN_BLOB_PIXEL_COUNT)
		{
			blobList->push_back(blobs[i]);
			blobs[i] = NULL;
		}
		else
		{
			delete blobs[i];
			blobs[i] = NULL;
		}
	}
	blobs.clear();
	delete[] m_Image;
	m_Image = NULL;
}

void CFindConnectedPixels::FindConnectedPixels(unsigned short *image, int width, int height, int threshold, vector<CBlobData *> *blobList)
{
	CleanUp();
	m_Threshold = (unsigned short)threshold;
	m_Width = width;
	m_Height = height;
	m_Image = image;
	m_Map = new unsigned short[width * height];
	memset(m_Map, 0, sizeof(unsigned short) * width * height);

	unsigned short currentLabel = (unsigned short)0;

	vector<CBlobData *> blobs;

	for (int i = 0; i < m_Height; i++)
	{
		for (int j = 0; j < m_Width; j++)
		{
			int position = m_Width * i + j; 
			if ((m_Map[position] == (unsigned short)0) && (image[position] >= m_Threshold))
			{
				currentLabel++;
				m_Map[position] = currentLabel;
				m_Stack.push(position);
				CBlobData *blob = new CBlobData(width, height);
				blob->m_Pixels->push_back(position);
				blob->m_Intensities->push_back(image[position]);
				blobs.push_back(blob);
				DoLabeling(blob);
			}
		}
	}

	for (int i = 0; i < (int)blobs.size(); i++)
	{
		if ((int)blobs[i]->m_Pixels->size() >= MIN_BLOB_PIXEL_COUNT)
		{
			blobList->push_back(blobs[i]);
			blobs[i] = NULL;
		}
		else
		{
			delete blobs[i];
			blobs[i] = NULL;
		}
	}
	blobs.clear();
	m_Image = NULL;
}

void CFindConnectedPixels::DoLabeling(CBlobData *blob)
{
	while (!m_Stack.empty())
	{
		int position = m_Stack.top();
		m_Stack.pop();
		int x0 = position % m_Width;
		int y0 = position / m_Width;
		unsigned short currentLabel = m_Map[m_Width * y0 + x0];
		int x1 = 0;
		int y1 = 0;
		for (int i = 0; i < 8; i++)
		{
			switch (i)
			{
			case 0:
				x1 = x0 - 1;
				y1 = y0 - 1;
				break;
			case 1:
				x1 = x0;
				y1 = y0 - 1;
				break;
			case 2:
				x1 = x0 + 1;
				y1 = y0 - 1;
				break;
			case 3:
				x1 = x0 + 1;
				y1 = y0;
				break;
			case 4:
				x1 = x0 + 1;
				y1 = y0 + 1;
				break;
			case 5:
				x1 = x0;
				y1 = y0 + 1;
				break;
			case 6:
				x1 = x0 - 1;
				y1 = y0 + 1;
				break;
			case 7:
				x1 = x0 - 1;
				y1 = y0;
				break;
			}

			if ((x1 >= 0) && (x1 < m_Width) && (y1 >= 0) && (y1 < m_Height))
			{
				int index = m_Width * y1 + x1;
				if ((m_Map[index] == (unsigned short)0) && (m_Image[index] >= m_Threshold))
				{
					m_Map[index] = currentLabel;
					m_Stack.push(index);
					blob->m_Pixels->push_back(index);
					blob->m_Intensities->push_back(m_Image[index]);
				}
			}
		}
	}
}

