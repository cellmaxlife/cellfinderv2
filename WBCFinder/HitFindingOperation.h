#pragma once
#include "ColorType.h"
#include "RGNData.h"
#include "CTCParams.h"
#include "SingleChannelTIFFData.h"
#include "Log.h"
#include <vector>
#include "BlobData.h"
#include "FindConnectedPixels.h"
#include "RegionDataSet.h"
#include "BlobTreeNode.h"

using namespace std;

#define SAMPLEFRAMEWIDTH  100
#define SAMPLEFRAMEHEIGHT 100

class CHitFindingOperation
{
protected:
	template<typename T> static void QuickSort(vector<T> *list, int lo, int hi);
	template<typename T> static void Swap(vector<T> *list, int index1, int index2);
	static int Partition(vector<int> *list, int lo, int hi);
	static int ChoosePivot(vector<int> *list, int lo, int hi);
	static int Partition(vector<CRGNData *> *list, int lo, int hi);
	static int ChoosePivot(vector<CRGNData *> *list, int lo, int hi);
	void FreeRGNList(vector<CRGNData *> *rgnList);
	void FreeBlobList(vector<CBlobData *> *blobList);
	static void FreeBlobTree(vector<CBlobTreeNode *> *blobTree);
	static void GetBlobBoundary(CBlobData *blob);
	static void CalculateBoundary(vector<CBlobData *> *blobList);
	static void ErosionOperation(unsigned char *inImage, unsigned char *outImage, int width, int height);
	static void FindBlobs(unsigned short *image, vector<CBlobData *> *blobs, int *CPI);
	static void SortPixelIntensities(unsigned short *image, vector<INTENSITY_PIXEL_SET *> *sortedList);
	static void FindBlob(CBlobTreeNode *node);
	static void AddOnePixel(vector<INTENSITY_PIXEL_SET *> *sortedList, unsigned short intensity, int position);
	static void PickLeafNodes(vector<CBlobTreeNode *> *treeHead, vector<CBlobTreeNode *> *leafNodes);
	bool MergeBlobsToGetCTC(CCTCParams *params, CRGNData *region, vector<CRGNData *> *rgnList);
	int GetOverlapPixelCount(vector<int> *blob1, vector<int> *blob2, vector<int> *blob3);
	bool blobIncluded(vector<int> *list, int blobIndex);
	void CalculateHitAttributes(CCTCParams *params, CRGNData *region);
	int GetPixelCountSum(CRGNData *region, PIXEL_COLOR_TYPE color);
	int GetAverageIntensity(CRGNData *region, PIXEL_COLOR_TYPE color);
	void CleanUpExclusionArea(CSingleChannelTIFFData *redImg, CSingleChannelTIFFData *greenImg, CSingleChannelTIFFData *blueImg);
	void SumUpImageData(unsigned short *image, unsigned short *sumImage, int width, int height);
	void ImageProjection(unsigned short *sumImage, int *profile, int width, int height, bool verticalProjection);
	int GetStepEdgePosition(int *profile[], int length, bool startFromBeginning);
	void LowpassFiltering(int *profile, int length);	
	bool ProcessOnePatch(CCTCParams *params, CRGNData *region, vector<CRGNData *> *rgnList);
	bool GetCellCenterLocation(CRGNData *region, int *xCenter, int *yCenter);
	bool PickARegionAtCenter(CRGNData *region, vector<CRGNData *> *regionList);

public:
	CHitFindingOperation();
	virtual ~CHitFindingOperation();
	CLog *m_Log;
	void ScanImage(CCTCParams *params, CSingleChannelTIFFData *redImage, CSingleChannelTIFFData *greenImage,
		CSingleChannelTIFFData *blueImage, vector<CRGNData *> *list, bool isBatchMode);
	bool ProcessOneRegion(CCTCParams *params, CRGNData *region, bool isZeissData);
	void ScanSingleFrame(CCTCParams *params, CSingleChannelTIFFData *redImage, CSingleChannelTIFFData *greenImage,
		CSingleChannelTIFFData *blueImage, vector<CRGNData *> *list, int FrameX0, int FrameY0);
	void SortRegionList(vector<CRGNData *> *regionList);
};

