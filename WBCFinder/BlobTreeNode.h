#pragma once
#include <vector>
#include "BlobData.h"

using namespace std;

class CBlobTreeNode
{
public:
	CBlobData *m_Node;
	int m_Threshold;
	vector<CBlobTreeNode *> *m_Children;

	CBlobTreeNode(CBlobData *blob, int threshold);
	~CBlobTreeNode();
};